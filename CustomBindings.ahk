﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

<#Enter::
Run powershell -Command "Start-Process PowerShell -Verb RunAs"
return

<#+Enter::
Run "C:\Program Files\Git\git-bash.exe"
return


<#b::
Run brave
return

#q::Send !{F4}
return

#s::
Run "C:\Program Files (x86)\Steam\steam.exe"
return


#d::Send #r
return

CapsLock::
Send {Escape}
return