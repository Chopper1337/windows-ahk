# Windows AutoHotKey Scripts

**You will need to create the 9 virtual desktops manually**

## Application Launching

`LeftWin + Enter` **PowerShell as Admin**

`LeftWin + Shift + Enter` **Git Bash**

`LeftWin + b` **Brave Browser** (Can easily be changed)

`LeftWin + s` **Steam**

`LeftWin + d` **Run Prompt**

## Window Management

`LeftWin + q` **Close window in focus**

`LeftWin + 1 ... 9` **Switch to Virtual Desktop 1 ... 9** (Including NumPad)

`LeftWin + Shift + 1 ... 9` **Move focused window to Virtual Desktop 1 ... 9** (Including NumPad)

`LeftWin + Tab` **Switch to the next Virtual Desktop**

`Ctrl + Tab` **Switch to the previous Virtual Desktop**

## Others

`CapsLock` sends `Escape`

## Credits

Virtual Desktop Switcher - https://github.com/pmb6tz/windows-desktop-switcher
